<?php
// Thanks to the inline module creator Matteo Ferrari for the base code for the module 
// and J. David Eisenberg for the ODTtoXHTML converter, modified by Michael Priest 2006.

function inline_odt_help($section = 'admin/help#inline') {
  $output = '';
  switch ($section) {
    case 'admin/help#inline_odt':
      return t('<p>Sometimes a user may want to add an image or a file inside the body of a node. This can be done with special tags that are replaced by links to the corresponding uploaded file. If the file is an open document text format, it will be display inline along with any images contained within that file. To enable this feature and learn the proper syntax, visit the <a href="%filters">filters configuration screen</a>.</p>', array('%filters' => url('admin/filters')));
    case 'admin/modules#description':
      return t('Allows users to convert uploaded odt files for display inline. Requires upload.module to work');
    case 'filter#short-tip':
      return t('You may add links to files uploaded with this node <a href="%explanation-url">using special tags</a>', array('%explanation-url' => url('filter/tips', NULL, 'image')));
    case 'filter#long-tip':
      return t('<p>You may link to files uploaded with the current node using special tags. The tags will be replaced by the corresponding files. Syntax: [inline_odt:file_id]. Parameter: file_id represents the file uploaded with the node in which to link, assuming that the first uploaded file is labeled as 1 and so on.</p>
    <p>The contents of the document will be converted and displayed inline, otherwise a link to the file will be inserted.</p> ');
  }
}

function inline_odt_settings() {
  $form = array();

  $form['inline_odt_link_odt'] = array(
    '#type' => 'radios',
    '#title' => t('Print link to documents'),
    '#default_value' => variable_get('inline_odt_link_odt', 1),
    '#options' => array('0' => t('Print only document contents'),'1' => t('Print document contents and the link to the document')),
  );

  return $form;
}

function inline_odt_form_alter($form_id, &$form) {
  if (isset($form['type'])) {
    if ($form['type']['#value'] .'_node_settings' == $form_id) {
      $form['workflow']['upload_inline_odt_'. $form['type']['#value']] = array(
          '#type' => 'radios',
          '#title' => t('Display attachments inline automatically'),
          '#default_value' => variable_get('upload_inline_odt_'. $form['type']['#value'], 0),
          '#options' => array(t('Disabled'), t('Only in teaser'), t('Only in body'), t('In teaser and body')),
          '#description' => t('Whether or not uploaded images should be shown inline. Make sure you set the dimensions at %settings_url', array('%settings_url' => l(t('inline settings'), 'admin/settings/inline'))),
      );
    }
  }
}

function inline_odt_filter($op, $delta = 0, $format = -1, $text = '') {
  // The "list" operation provides the module an opportunity to declare both how
  // many filters it defines and a human-readable name for each filter. Note that
  // the returned name should be passed through t() for translation.
  if ($op == 'list') {
    return array(
      0 => t('Inline open document file filter'));
  }

  // All operations besides "list" provide a $delta argument so we know which
  // filter they refer to. We'll switch on that argument now so that we can
  // discuss each filter in turn.
  switch ($op) {
    case 'description':
       return t('Substitutes [inline_odt:xx] tags with the xxth file uploaded with the node.');
    case 'prepare':
      return $text;
      break;
    case 'process':
      return $text;
      break;
  }
}

function inline_odt_filter_tips($delta, $format, $long = false) {
  if ($long) {
    return t('
    <p>You may link to files uploaded with the current node using special tags. The tags will be replaced by the corresponding files. For example:

    Suppose you uploaded three files (in this order):
    <ul>
    <li>TermsAndConditions.odt (referred as file #1)
    <li>Story.odt (referred as file #2)
    <li>Diary.odt (referred as file #3)
    </ul>

    [inline_odt:1=test]  or  [inline_odt:TermsAndConditions.odt=test]

    will be replaced by the converted contents of the file


    [inline_odt:2]  or  [inline_odt:Story.odt]

    will be replaced by the converted contents of the file


    [inline_odt:3=comment]  or  [inline_odt:Diary.odt=comment]

    will be replaced by the converted contents of the file

    ');
  }
  else {
    return t('You may use <a href="%inline_odt_help">[inline_odt:xx] tags</a> to display uploaded files or images inline.', array("%inline_odt_help" => url("filter/tips/$format", NULL, 'filter-inline')));
  }
}

function inline_odt_nodeapi(&$node, $op, $arg) {
  if(is_array($node->files) && $op == 'view') {
    //TODO: we should *really* us the filter hooks for this, not?
    $node->teaser = _inline_odt_substitute_tags($node, 'teaser');
    $node->body = _inline_odt_substitute_tags($node, 'body');
  }
}

function _inline_odt_fileobj(&$node, $id) {
  if (is_numeric($id)) {
    $n=1;
    foreach ($node->files as $file) {
      if ($n == $id) {
        return $file;
      }
      ++$n;
    }
    return NULL;
  }
  else
  {
    foreach ($node->files as $file) {
      if ($file->filename == $id) {
        //return array($file->filename, $file->filepath);
        return $file;
      }
    }
    return NULL;
  }
}

function theme_inline_odt_as_link($file) {
  return l(($file->description ? $file->description : $file->name),
    file_create_url($file->filepath),
    array('title' => t('Download: %name (%size)',
      array('%name' => $file->filename, '%size' => format_size($file->filesize)))));
}

function theme_inline_odt_odt($file) {
    
  include_once('odt2xhtml.php');
  $class = new ODT2XHTML;
	
  if (variable_get('inline_odt_link_odt', '1') == '1') {
    $html .= '<a href="'. file_create_url($file->filepath) . '" title="'.t("View").': '. $file->filename .'">' .
      $file->filename .
      '</a>';
  }
   
  // Needs attention!!!
  // zip_open function (domxml extension) needs an absolute path under win32 (not 100% sure on linux)
  $html .= $class->oo_convert($class->oo_unzip($_SERVER['DOCUMENT_ROOT'] . base_path() . $file->filepath));

  return $html;
}

function theme_inline_odt_add_to_teaser($node, $file) {
   return theme('inline_odt_odt', $file) . $node->teaser;
}

function theme_inline_odt_add_to_body($node, $file) {
   return theme('inline_odt_odt', $file) . $node->body;
}

function _inline_odt_substitute_tags(&$node, $field) {
  if (preg_match_all("/\[(inline_odt):([^=\\]]+)=?([^\\]]*)?\]/i", $node->$field, $match)) {
    foreach ($match[2] as $key => $value) {
      $map[$value] = $key;
      $titl = $match[3][$key];
      $ytype = $match[1][$key];
      $file = _inline_odt_fileobj($node, $value);
      $replace = "";
      if ($file->fid != NULL) {
        //decide if we should show a link or converted (x)html
        if (_inline_odt_decide_odt_tag($file)) {
          $replace = theme('inline_odt_odt', $file);
        }
        else {
          $replace = theme('inline_odt_as_link', $file);
        }
      }
      else {
        $replace = "<span style=\"color:red; font-weight:bold\">COULD NOT FIND ATTACHMENT: $value - check that it exists in the attachment list below.</span>";
      }
      $mtch[] = $match[0][$key];
      $repl[] = $replace;
    }
    return str_replace($mtch, $repl, $node->$field);
  }
  return $node->$field;
}

/**
 * Decides if a tag (&lt;img&gt;) or a link to a file should be rendered
 * @param $file a file object
 * @return TRUE in case the file really is odt.
 */
function _inline_odt_decide_odt_tag($file) {
  $inlined = array('vnd.oasis.opendocument.text');
  $mime = array_pop(explode('/', $file->filemime));
  if (in_array($mime, $inlined)) {
      return TRUE;
  }
  return FALSE;
}